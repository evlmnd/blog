import React, {Component, Fragment} from 'react';

import './Blog.css';
import Post from "../../components/Post/Post";

class Blog extends Component {

    state = {
        posts: [
            // {title: 'Test Post', author: 'John', id: 1},
            // {title: 'Hello Post', author: 'Joooohn', id: 2},
            // {title: 'Example Post', author: 'Johnnnyyy', id: 3}
        ],
        postsFormShown: false
    };

    constructor(props) {
        super(props);
        console.log('([Blog]) constructor');
        console.log('[Blog] state exists: ', this.state.posts.length > 0);
    };

    _makeRequest (url) {
        return fetch(url).then(response => {
            if (response.ok) {
                return response.json();
            }

            throw new Error('Error');
        })
    };

    componentDidMount() {
        console.log('[Blog] didMount');

        const BASE_URL = 'http://jsonplaceholder.typicode.com';
        const POSTS_URL = '/posts?_limit=4';
        const USER_URL = '/users/';

        this._makeRequest(BASE_URL + POSTS_URL).then(posts => {
            const promises = Promise.all(posts.map(post => {
                return this._makeRequest(BASE_URL + USER_URL + post.userId);
            }));

            // const updatedPosts = posts.map(post => {
            //     return {
            //         ...post,
            //         author: 'john'
            //     };
            // });

            this.setState({posts: updatedPosts});
        }).catch(error => {
            console.log(error);
        })
    };

    togglePostsForm = () => {
        this.setState(prevState => {
            console.log('[Blog] toggling form');
            return {postsFormShown: !prevState.postsFormShown}
        });
    };

    componentDidUpdate() {
        console.log('[Blog] didUpdate');
    }

    render() {
        console.log('[Blog] render');

        let postsForm = null;

        if (this.state.postsFormShown) {
            postsForm = <section className="NewPost">New Post form</section>
        }

        return (
            <Fragment>
                <section className="Posts">
                    {this.state.posts.map(post => (
                        <Post
                            key={post.id}
                            title={post.title}
                            author={post.author}
                        />
                    ))}
                </section>
                <button onClick={this.togglePostsForm}>NewPost</button>
                {postsForm}
            </Fragment>
        );
    }
}

export default Blog;