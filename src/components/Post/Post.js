import React, {PureComponent} from 'react';
import './Post.css';

class Post extends PureComponent {
    componentDidMount() {
        console.log('[Post] didMount');
    }

    componentDidUpdate() {
        console.log('[post] didUpdate');
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     console.log('[Post] shouldUpdate');
    //
    //     return nextProps.title !== this.props.title ||
    //         nextProps.author !== this.props.author;
    // }

    render() {
        console.log('[Post] render');
        return (
            <article className="Post">
                <h1>{this.props.title}</h1>
                <div className="Info">
                    <div className="Author">{this.props.author}</div>
                </div>
            </article>
        );
    }
}

export default Post;