import React, { Component } from 'react';
import Blog from './containers/Blog/Blog';
import './App.css';

class App extends Component {
  render() {
    return <Blog/>;
  }
}

export default App;
